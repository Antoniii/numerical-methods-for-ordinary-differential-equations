import matplotlib.pyplot as plt
import numpy as np

# Создаём экземпляр класса figure и добавляем к Figure область Axes
fig, ax = plt.subplots()
# Добавим заголовок графика
ax.set_title(r'$y(x) = \frac{3}{2}e^{2x} - x^2 - x - \frac{1}{2}$')
# Название оси X:
ax.set_xlabel('x')
# Название оси Y:
ax.set_ylabel('y')
# Начало и конец изменения значения X, разбитое на 100 точек
x = np.linspace(0, 1, 100) # X от 0 до 1
# Построение прямой
y = 1.5*np.exp(2*x) - x**2 - x - 0.5
# Вывод графика
ax.plot(x, y)
ax.grid() # сетка
plt.savefig("ideal-plot1.png")
plt.show()