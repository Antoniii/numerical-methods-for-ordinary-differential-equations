![](https://gitlab.com/Antoniii/numerical-methods-for-ordinary-differential-equations/-/raw/main/teor12.PNG)

![](https://gitlab.com/Antoniii/numerical-methods-for-ordinary-differential-equations/-/raw/main/dydx.PNG)

![](https://gitlab.com/Antoniii/numerical-methods-for-ordinary-differential-equations/-/raw/main/ideal-plot.png)

![](https://gitlab.com/Antoniii/numerical-methods-for-ordinary-differential-equations/-/raw/main/from-python.png)

![](https://gitlab.com/Antoniii/numerical-methods-for-ordinary-differential-equations/-/raw/main/from-C.png)

![](https://gitlab.com/Antoniii/numerical-methods-for-ordinary-differential-equations/-/raw/main/teorVan.PNG)

![](https://gitlab.com/Antoniii/numerical-methods-for-ordinary-differential-equations/-/raw/main/Van_Der_Pol.png)

![](https://gitlab.com/Antoniii/numerical-methods-for-ordinary-differential-equations/-/raw/main/Van_Der_Pol1.png)

## Sources

* [Численное решение обыкновенных дифференциальных уравнений (ОДУ) в Python](http://spyphy.zl3p.com/python/34_scipy_ode)
* [Задача Коши для ОДУ методом Эйлера - C (СИ)](https://www.cyberforum.ru/c-beginners/thread2884511.html)
* [Решение дифференциальных уравнений 1 порядка методом Эйлера - C (СИ)](https://studassistent.ru/c/reshenie-differencialnyh-uravneniy-1-poryadka-metodom-eylera-c-si)
* [Колебания. Колебательные системы. Модели колебательных систем на примере дифференциальных уравнений](https://www.numamo.org/HTML/Articles/Oscillator.html)
* [Метод медленно меняющихся амплитуд (метод Ван-дер-Поля)](https://lfirmal.com/metod-medlenno-menyayushchihsya-amplitud-metod-van-der-polya/)
* [Работа с файлами (Язык Си)](https://prog-cpp.ru/c-files/)
* [Solving Van der Pol equation with ivp_solve](https://www.johndcook.com/blog/2019/12/22/van-der-pol/)
* [LaTeX - формулы](https://pyprog.pro/mpl/mpl_latex_title.html)
* [Scatter - график разброса (точки)](https://pyprog.pro/mpl/mpl_scatter.html)
* [Построение графиков функций на python](https://it-start.online/articles/postroenie-grafikov-funkcij-na-python)
